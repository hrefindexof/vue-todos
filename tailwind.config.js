import plugin from 'tailwindcss/plugin'

const rem = val => `${val / 16}rem`

export default {
	content: ['./index.html', './src/**/*.{vue,js}'],
	theme: {
		screens: {
			xl: { max: '1921px' },
			// => @media (max-width: 1279px) { ... }
			xll: { max: '1800px' },
			lg: { max: '1600px' },
			// => @media (max-width: 1023px) { ... }
			lg_b: { max: '1450px' },

			lg_s: { max: '1350px' },

			md: { max: '992px' },
			// => @media (max-width: 767px) { ... }
			md_min: '992px',

			sm: { max: '639px' },
			// => @media (max-width: 639px) { ... }
			def: '1810px',
		},
		extend: {
			colors: {
				'color-a': '#CDCEC9', //!primary
				'color-b': '#01AC9A', //!primary-hover
				'color-c': '', //!secondary
				'color-d': '',
				'color-e': '',
				'color-f': '',
				'color-g': '',
				'white-black': '#fff',
				'black-white': '#000',
				'color-border': 'rgba(205, 206, 201, 0.20)',
			},
			transitionProperty: {
				'bg-color': 'background-color',
			},
			fontSize: {
				xs: rem(8),
				sm: rem(14),
				base: rem(16),
				lg: rem(24),
				xl: rem(32),
				'2xl': rem(42),
				'3xl': rem(52),
				'4xl': rem(54),
				'5xl': '3.5rem',
				'6xl': rem(60),
				'7xl': rem(80),
				'8xl': rem(100),
			},
			btnSize: {
				'xs-x': rem(100),
				'xs-y': rem(50),
				'sm-x': rem(150),
				'sm-y': rem(50),
				'base-x': rem(200),
				'base-y': rem(50),
				'lg-x': rem(250),
				'lg-y': rem(50),
				'xl-x': rem(300),
				'xl-y': rem(50),
				'2xl-x': rem(400),
				'2xl-y': rem(22),
				'3xl-x': rem(22),
				'3xl-y': rem(22),
				'4xl-x': rem(22),
				'4xl-y': rem(22),
				'5xl-x': rem(22),
				'5xl-y': rem(22),
			},
			iconSize: {
				xs: rem(12),
				sm: rem(40),
				base: rem(50),
				lg: rem(60),
				xl: rem(100),
				'2xl': rem(222),
				'3xl': rem(38),
				'4xl': rem(40),
				'5xl': rem(56),
			},
		},
		content: {
			arrowRightIcon: 'url("/svg/arrowRightIcon.svg")',
		},
		fontFamily: {
			fontSecond: ['NeutralFaceLight', 'system-ui'],
			// fontSecond: ['', '']
		},
	},
	future: {
		hoverOnlyWhenSupported: true,
	},
	plugins: [
		plugin(function ({ addVariant }) {
			addVariant('not-last', '&:not(:last-child)')
		}),
	],
}
