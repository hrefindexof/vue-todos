import Routing from './index.vue'

export const routes = [
	{
		path: '/',
		name: 'main',
		meta: {},
		component: () => import('./main'),
	},
	{
		path: '/privacy-policy/',
		name: 'privacy-policy',
		meta: {
			title: 'Политика конфиденциальности',
		},
		component: () => import('./privacy-policy'),
	},
	{
		path: '/:pathMatch(.*)*/',
		name: 'not-found',
		meta: {},
		component: () => import('./page-not-found'),
	},
]

export { Routing }
