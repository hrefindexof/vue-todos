import { createApp } from 'vue'
import App from './index.vue'

import { router } from './router'

import 'normalize.css'
import './index.scss'

export const app = createApp(App).use(router)
