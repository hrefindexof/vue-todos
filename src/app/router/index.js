import { createRouter, createWebHistory } from 'vue-router'
import { routes } from '@/pages'

export const router = createRouter({
	history: createWebHistory(),
	routes,
})

router.beforeEach((to, from, next) => {
	document.title = to.meta.title || 'ToDoList'

	// Проверка на наличие query в route в который стучимся
	// if (!Object.keys(to.query).length) {
	self.scrollTo({
		top: 0,
		behavior: 'smooth',
	})

	next()
})
