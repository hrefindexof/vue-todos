import axios from 'axios'

const env = import.meta.env

axios.defaults.baseURL = location.origin + env.VITE_BASEURL
